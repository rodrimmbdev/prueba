#include <stdio.h>


void saluda(){
	printf("Hola\n");	
}

void quieresSaludo(){
	printf("¿Quieres saludo?(y/n)\n");		
}

int main(){
	char entrar;
	
	quieresSaludo();
	scanf("%c", &entrar);
	if(entrar == 'y'){
		saluda();
	}
	return 0;
}
